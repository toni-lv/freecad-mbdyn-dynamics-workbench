# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################


from matplotlib import pyplot as plt
import numpy as np
import FreeCADGui, FreeCAD
import tempfile


class Plotabstract:  
    def __init__(self, node):
        
        __dir1__ = tempfile.gettempdir()
        
        inittime = FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].initial_time.Value
        endtime = FreeCAD.ActiveDocument.getObjectsByLabel('MBDyn')[0].final_time.Value

        data = np.genfromtxt(__dir1__ + '/MBDynCase.abs', delimiter=' ')
        
        aux = data[:,0]
        aux1 = data[:,1]

        ys = []        
        
        for d in range(len(aux)):

            if aux[d] == int(node.label):
                ys.append(aux1[d])
                
        xs = np.linspace(inittime, endtime, num=len(ys))
                
        fig, ax = plt.subplots()
        ax.plot(xs, ys)
        
        ax.set(xlabel='Time', ylabel='f(Time)', title = FreeCADGui.Selection.getSelection()[0].Label)
        ax.grid()
        
        plt.show()



