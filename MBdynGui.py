# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
import FreeCADGui
#from PyQt4 import QtGui,QtCore
from PySide import QtGui,QtCore
import os
#import subprocess
from tospreadsheet import Tospreadsheet
from info import Infonode, Infojoint
from dynamics import Dynamics
#from sys import platform
#from customview import CustomView

dyn = Dynamics()

__dir__ = os.path.dirname(__file__)


############################################################################################################################################################
#
#  MEASURING TOOLS:
#    
############################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////MEASURE LINEAR DISTANCE BETWEEN TWO NODES////////////////////////////////////////////////////////"""
class _MeasureLinearDistanceBetweenNodesCmd:   
    def Activated(self):
        #try:
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
                                   
        dyn.MeasureLinearDistanceBetweenNodes(node1, node2) 
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the drive hinge joint.
                                          
#Please make sure you have selected only two nodes.""")              

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'MEASURE LINEAR DISTANCE BETWEEN TWO NODES',
            'MEASURE LINEAR DISTANCE BETWEEN TWO NODES')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'MEASURE LINEAR DISTANCE BETWEEN TWO NODES',
            """<pre>SELECT TWO NODES TO MEASURE THE LINEAR DISTANCE BETWEEN THEM.\n
    Note: the distance will be shown as a message in the python console. 
    </pre>""")
        return {
            'Pixmap': __dir__ + '/icons/tape.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_MeasureLinearDistanceBetweenNodes', _MeasureLinearDistanceBetweenNodesCmd()) 

############################################################################################################################################################
#
#  JOINTS:
#    
############################################################################################################################################################
"""//////////////////////////////////////////////////////////////////////////////DISTANCE JOINT////////////////////////////////////////////////////////"""
class _AddDistanceJointCmd:   
    def Activated(self):
        #try:
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
                                   
        dyn.AddDistanceJoint(node1, node2) 
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the drive hinge joint.
                                          
#Please make sure you have selected only two nodes.""")              

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'DISTANCE JOINT',
            'DISTANCE JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'DISTANCE JOINT',
            """<pre>SELECT TWO NODES TO ADD A DISTANCE JOINT.\n
    This joint forces the distance between two nodes to assume the value indicated by the drive. 
    </pre>""")
        return {
            'Pixmap': __dir__ + '/icons/distance.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDistanceJoint', _AddDistanceJointCmd()) 

"""//////////////////////////////////////////////////////////////////////////////DRIVE HINGE JOINT////////////////////////////////////////////////////////"""
class _AddDrivehingeCmd:   
    def Activated(self):
        #try:
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
                 
        referenceObject = FreeCADGui.Selection.getSelection()[2]
        referenceGeometry = FreeCADGui.Selection.getSelectionEx()[2]   
                  
        dyn.AddDriveHingeJoint(node1, node2, referenceObject, referenceGeometry) 
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the drive hinge joint.
                                          
#Please make sure you have selected only two nodes.""")              

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'DRIVE-HINGE JOINT',
            'DRIVE-HINGE JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'DRIVE-HINGE JOINT',
            """<pre>SELECT TWO NODES AND ONE REFERENCE GEOMETRY, IN THIS ORDER, TO CREATE A DRIVE HINGE JOINT.\n
    This joint imposes the relative orientation between two nodes, in the form of a rotation about a given axis, 
    which passes through the first node and the center of mass (or the center point, in the case of arcs), of the
    reference provided.    
    The amplitude of the rotation is imposed by means of a driver, a scalar function, and/or a mathematical expression. 
    This expression may include user-defined variables and plugin variables. You will have to provide an expression 
    for the rotation amplitude after creating the joint.\n
    Note: drive hinge joints are experimental; now they are more reliable, but are limited to "hinge_orientation" < pi. 
    Note: This element is superseded by the total joint.
    Note: This element is supposed to be used in conjunction with an spherical hinge joint.
    </pre>""")
        return {
            'Pixmap': __dir__ + '/icons/drivehinge.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDrivehinge', _AddDrivehingeCmd()) 

"""//////////////////////////////////////////////////////////////////////////////REVOLUTE PIN JOINT////////////////////////////////////////////////////"""
class _AddRevolutepinCmd:   
    def Activated(self): 
        #try:                                   
        node = FreeCADGui.Selection.getSelection()[0]
        if len(FreeCADGui.Selection.getSelection()) == 2:
            obj1 = FreeCADGui.Selection.getSelection()[1]
            obj2 = obj1
            reference1 = FreeCADGui.Selection.getSelectionEx()[1]
            reference2 = reference1
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            obj1 = FreeCADGui.Selection.getSelection()[1]
            obj2 = FreeCADGui.Selection.getSelection()[2]
            reference1 = FreeCADGui.Selection.getSelectionEx()[1]
            reference2 = FreeCADGui.Selection.getSelectionEx()[2]
            
        dyn.AddRevolutePin(node, obj1, obj2, reference1, reference2) 
        #except:
            #QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the revolute pin joint.
                                          
#Please meke sure you have selected one node and two valid reference geometries, in this order.""")
                     
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'REVOLUTE PIN JOINT',
            'REVOLUTE PIN JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'REVOLUTE PIN JOINT',
            """<pre>SELECT ONE NODE AND TWO VALID REFERENCE GEOMETRIES, IN THIS ORDER, TO CREATE A REVOLUTE PIN JOINT.\n
    This joint only allows the absolute rotation of a node about a given axis, which is fixed to ground. 
    The rotation axis is defined by the line passing through the center of mass (or the center point, in
    the case of arcs), of the two reference geometries provided.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/hinge.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRevolutepin', _AddRevolutepinCmd()) 

"""//////////////////////////////////////////////////////////////////////////////REVOLUTE HINGE JOINT///////////////////////////////////////////////////"""
class _AddRevolutehingeCmd:   
    def Activated(self):
        #try:                
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = referenceObject1
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = referenceGeometry1
        
        if len(FreeCADGui.Selection.getSelection()) == 4:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = FreeCADGui.Selection.getSelection()[3]
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]    
            
        dyn.AddRevoluteHingeJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2)              
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the revolute hinge joint.
                                          
#Please make sure you have selected two nodes and two valid reference geometries, in this order.""")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'REVOLUTE HINGE JOINT',
            'REVOLUTE HINGE JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'REVOLUTE HINGE JOINT',
            """<pre>SELECT TWO NODES AND TWO VALID REFERENCE GEOMETRIES, IN THIS ORDER, TO CREATE A REVOLUTE HINGE JOINT.\n
    This joint only allows the relative rotation of two nodes about a given axis, which is not fixed to ground.
    The rotation axis is defined by the line passing through the center of mass (or the center point, in the 
    case of arcs), of the two reference geometries provided.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/hinge1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRevolutehinge', _AddRevolutehingeCmd()) 

"""//////////////////////////////////////////////////////////////////////////////CLAMP JOINT////////////////////////////////////////////////////////////////"""
class _AddClampCmd:    
    def Activated(self):
        #try:
        dyn.AddClampJoint()
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the revolute clamp joint.
                                          
#Please make sure you have selected only one node.""")        

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'CLAMP JOINT',
            'CLAMP JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'CLAMP JOINT',
            """<pre>SELECT ONLY ONE NODE TO CREATE A CLAMP JOINT.\n
    This joint grounds all 6 degrees of freedom of a node 
    in an arbitrary position and orientation.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/clamp.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddClamp', _AddClampCmd())  

"""//////////////////////////////////////////////////////////////////////////////AXIAL ROTATION JOINT//////////////////////////////////////////////////////"""
class _AddAxialRotationCmd:   
    def Activated(self):
        #try:
         node1 = FreeCADGui.Selection.getSelection()[0]
         node2 = FreeCADGui.Selection.getSelection()[1]
         
         if len(FreeCADGui.Selection.getSelection()) == 3:
             referenceObject1 = FreeCADGui.Selection.getSelection()[2]
             referenceObject2 = referenceObject1
             referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
             referenceGeometry2 = referenceGeometry1
         
         if len(FreeCADGui.Selection.getSelection()) == 4:
             referenceObject1 = FreeCADGui.Selection.getSelection()[2]
             referenceObject2 = FreeCADGui.Selection.getSelection()[3]
             referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
             referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]    
             
         dyn.AddAxialRotationJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2)             
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the axial rotation joint.
                                          
#Please make sure you have selected two nodes and two reference geometries.""")          
                                
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'AXIAL ROTATION JOINT',
            'AXIAL ROTATION JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'AXIAL ROTATION JOINT',
            """<pre>SELECT TWO NODES AND TWO VALID REFERENCE GEOMETRIES, IN THIS ORDER, TO CREATE AN AXIAL ROTATION JOINT.\n
    This joint is equivalent to a revolute hinge joint, but the angular velocity about the rotation axis is imposed by 
    means of a driver, a scalar function, and/or a mathematical expression. This expression may include user-defined variables and plugin variables. 
    You will have to provide an angular velocity after creating the joint.
    The rotation axis is defined by the line passing through the two centers of mass (or the center points, in the case 
    of arcs), of the two reference geometries provided.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/axial.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AxialRotation', _AddAxialRotationCmd()) 

"""//////////////////////////////////////////////////////////////////////////////IN-LINE JOINT///////////////////////////////////////////////////////////"""
class _AddInLineCmd:   
    def Activated(self):
        #try:
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = referenceObject1
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = referenceGeometry1
        
        if len(FreeCADGui.Selection.getSelection()) == 4:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = FreeCADGui.Selection.getSelection()[3]
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]    
            
        dyn.AddInLineJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2)        

        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the in-line joint.
                                          
#Please make sure you have selected two nodes and two valid reference geometries, in this order.""")             
                
    def GetResources(self):        

        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'IN-LINE JOINT',
            'IN-LINE JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'IN-LINE JOINT',
            """<pre>SELECT TWO NODES AND TWO VALID REFERENCE GEOMETRIES, IN THIS ORDER, TO CREATE AN IN-LINE JOINT.\n
    This joint forces a point rigidly attached to the second node, to move along a line rigidly attached to the first node.
    The line is defined by the two centers of mass (or the center points, in the case of arcs), of the two reference 
    geometries provided.
    
    Note: The absolute orientation of the second node is not constrained.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/in-line.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddInLine', _AddInLineCmd()) 

"""//////////////////////////////////////////////////////////////////////////////PRISMATIC JOINT////////////////////////////////////////////////////////"""
class _AddPrismaticCmd:   
    def Activated(self):
        try:
            dyn.AddPrismaticJoint() 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the prismatic joint.
                                          
Please make sure you have selected only two nodes.""")            
                              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'PRISMATIC JOINT',
            'PRISMATIC JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'PRISMATIC JOINT',
            """<pre>SELECT TWO NODES TO CREATE A PRISMATIC JOINT.\n        
    This joints constrains the relative orientation of two nodes, so that they remain parallel.
    The relative position is not constrained.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/prismatic.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddPrismatic', _AddPrismaticCmd()) 

"""//////////////////////////////////////////////////////////////////////////////DEFORMABLE DISPLACEMENT JOINT////////////////////////////////////////////////////"""
class _AddDeformableDisplacementCmd:   
    def Activated(self):
        #try:
                        
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = referenceObject1
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = referenceGeometry1
        
        if len(FreeCADGui.Selection.getSelection()) == 4:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = FreeCADGui.Selection.getSelection()[3]
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]    
            
        dyn.AddDeformableDisplacementJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2) 
                    
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the deformable displacement joint.  
                                          
#Please make sure you have selected two nodes and, optionally, two reference geometries.""")                                           
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'DEFORMABLE DISPLACEMENT JOINT',
            'DEFORMABLE DISPLACEMENT JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'DEFORMABLE DISPLACEMENT JOINT',
            """<pre>SELECT TWO NODES AND TWO REFERENCE GEOMETRIES TO ADD A DEFORMABLE DISPLACEMENT JOINT.\n
    This joint implements a configuration dependent force that is exchanged between two points associated
    to two nodes with an offset. If only two nodes are selected, these offsets become zero, and the points 
    are located at the nodes. The force may depend, by means of a generic 3D constitutive law, on the 
    relative position and velocity of the two points, expressed in the reference frame of node 1.\n
    The constitutive law is attached to the reference frame of node 1, so the sequence of the connections
    may matter in case of anisotropic constitutive laws, if the relative orientation of the two nodes 
    changes during the analysis.\n
    You will have to provide a constitutive law after creating the joint.
    </pre>""")
        return {
            'Pixmap': __dir__ + '/icons/spring.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_DeformableDisplacement', _AddDeformableDisplacementCmd())

"""//////////////////////////////////////////////////////////////////////////////SPHERICAL HINGE///////////////////////////////////////////////////////////////////"""
class _AddSphericalHingeCmd: 
    def Activated(self):
        try:
            dyn.AddSphericalHinge()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the spherical hinge joint.  
                                          
Please make sure you have selected two nodes.""")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'SPHERICAL HINGE JOINT',
            'SPHERICAL HINGE JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'SPHERICAL HINGE JOINT',
            """<pre>SELECT TWO NODES TO CREATE A SPHERICAL HINGE JOINT.\n
    This joint constrains the relative position of two nodes, so that the second node is forced to move on a sphere 
    whose center is attached to the first node.\n 
    The relative orientation is not constrained.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/spherical.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddSphericalHinge', _AddSphericalHingeCmd()) 

"""//////////////////////////////////////////////////////////////////////////////IN-PLANE JOINT////////////////////////////////////////////////////////////////////"""
class _AddInPlaneCmd:   
    def Activated(self):
        #try:
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = referenceObject1
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = referenceGeometry1
        
        if len(FreeCADGui.Selection.getSelection()) == 4:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = FreeCADGui.Selection.getSelection()[3]
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]    
            
        dyn.AddInPlaneJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2)               
        #except:
         #   QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the in-plane joint.  
                                          
#Please make sure you have selected two nodes.""")                
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'IN-PLANE JOINT',
            'IN-PLANE JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'IN-PLANE JOINT',
            """<pre>SELECT TWO NODES AND TWO REFERENCE GEOMETRIES TO CREATE AN IN-PLANE JOINT.\n
    This joint forces a point rigidly attached to the second node, to move in a plane attached to the first node. 
    The plane is normal to the line passing through the two reference geometries provided.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/in-plane.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddInPlane', _AddInPlaneCmd()) 

"""//////////////////////////////////////////////////////////////////////////////TOTAL JOINT////////////////////////////////////////////////////"""
class _AddTotalJointCmd:   
    def Activated(self):
        #try:
        dyn.AddTotalJoint() 
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the total joint.  
                                          
#Please make sure you have selected two nodes.""")          

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'TOTAL JOINT',
            'TOTAL JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'TOTAL JOINT',
            """<pre>SELECT TWO NODES TO CREATE A TOTAL JOINT.\n
    This joint allows to arbitrarily constrain specific components of the relative position
    and orientation of two nodes. The value of the constrained components of the relative 
    position and orientation is imposed by means of a driver, a scalar function, and/or 
    a mathematical expression. This expression may include user-defined variables and 
    plugin variables.
    This element allows to mimic the behavior of most ideal constraints that connect two nodes.\n
    NOTE: The order in which the nodes are selected matters: the second node is driven by the first node.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/total.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddTotalJoint', _AddTotalJointCmd())

"""//////////////////////////////////////////////////////////////////////////////TOTAL PIN JOINT////////////////////////////////////////////////////"""
class _AddTotalPinJointCmd:   
    def Activated(self):
        try:
            dyn.AddTotalPinJoint() 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the total pin joint.  
                                          
Please make sure you have selected two nodes.""")          

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'TOTAL PIN JOINT',
            'TOTAL PIN JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'TOTAL PIN JOINT',
            """<pre>SELECT ONE NODE TO CREATE A TOTAL PIN JOINT.\n
    This joint allows to arbitrarily constrain specific components of the absolute position and orientation
    of a node. The value of the constrained components of the absolute position and orientation is imposed by 
    means of a driver, a scalar function, and/or a mathematical expression. This expression may include 
    user-defined variables and plugin variables. 
    This element allows to mimic the behavior of most ideal constraints that ground one node.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/totalpin.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddTotalPinJoint', _AddTotalPinJointCmd())


"""//////////////////////////////////////////////////////////////////////////////REVOLUTE ROTATION JOINT////////////////////////////////////////////////////"""
class _AddRevoluterotationCmd:   
    def Activated(self):

        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = referenceObject1
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = referenceGeometry1
        
        if len(FreeCADGui.Selection.getSelection()) == 4:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = FreeCADGui.Selection.getSelection()[3]
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]    
            
        dyn.AddRevoluteRotation(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2)                                        

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'REVOLUTE ROTATION JOINT',
            'REVOLUTE ROTATION JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'REVOLUTE ROTATION JOINT',
            """<pre>SELECT TWO NODES AND TWO REFERENCE GEOMETRIES TO CREATE A REVOLUTE ROTATION JOINT.\n
    This joint only allows the relative rotation of two nodes about a given axis. The relative position is not constrained.
    The rotation axis is defined by the line passing through the center of mass (or center point in the case of arcs), of 
    the two reference geometries provided.\n
    Note: This joint is equivalent to a revolute joint without position constraints. In conjunction with an in-line joint, 
    this joint should be used to constrain, for example, the two nodes of a hydraulic actuator (see the cylindrical 
    "compound" joint").</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/revoluterotation.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRevoluterotation', _AddRevoluterotationCmd()) 


############################################################################################################################################################
#
#  COMPOUND JOINTS:
#    
############################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////CONCIDENCE JOINT////////////////////////////////////////////////////"""
class _AddLockCmd:   
    def Activated(self):
        try:
            dyn.AddLockJoint()   
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the coincidence joint.
                                          
Please make sure you have selected two nodes.""")  
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'COINCIDENCE JOINT',
            'COINCIDENCE JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'COINCIDENCE JOINT',
            """<pre>SELECT TWO NODES TO ADD A COINCIDENCE JOINT.\n
   A spherical hinge joint and a prismatic joint will be added, restricting all 6 degrees of freedom.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/lock.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddLock', _AddLockCmd())  

"""//////////////////////////////////////////////////////////////////////////////CYLINDRICAL JOINT////////////////////////////////////////////////////"""
class _AddCylindricalCmd:   
    def Activated(self):
        #try:
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = referenceObject1
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = referenceGeometry1
        
        if len(FreeCADGui.Selection.getSelection()) == 4:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = FreeCADGui.Selection.getSelection()[3]
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]                
        
        dyn.AddCyindricalJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2)   
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the cylindrical joint.
                                          
#Please make sure you have selected two nodes and two valid reference geometries.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'CYLINDRICAL JOINT',
            'CYLINDRICAL JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'CYLINDRICAL JOINT',
            """<pre>SELECT TWO NODES AND TWO REFERENCE GEOMETRIES TO CREATE A CYLINDRICAL JOINT.\n
   An in-line joint and a revolute rotation joint will be added.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/cylindrical.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddCylindrical', _AddCylindricalCmd())  

"""//////////////////////////////////////////////////////////////////////////////SLIDER JOINT//////////////////////////////////////////////////////////"""
class _AddSliderCmd:   
    def Activated(self):
        #try:
        node1 = FreeCADGui.Selection.getSelection()[0]
        node2 = FreeCADGui.Selection.getSelection()[1]
        
        if len(FreeCADGui.Selection.getSelection()) == 3:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = referenceObject1
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = referenceGeometry1
        
        if len(FreeCADGui.Selection.getSelection()) == 4:
            referenceObject1 = FreeCADGui.Selection.getSelection()[2]
            referenceObject2 = FreeCADGui.Selection.getSelection()[3]
            referenceGeometry1 = FreeCADGui.Selection.getSelectionEx()[2]
            referenceGeometry2 = FreeCADGui.Selection.getSelectionEx()[3]                
        
        #dyn.AddCyindricalJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2) 
        dyn.AddSliderJoint(node1, node2, referenceObject1, referenceObject2, referenceGeometry1, referenceGeometry2)    
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the slider joint.
                                          
#Please make sure you have selected two nodes and two valid reference geometries.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'SLIDER JOINT',
            'SLIDER JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'SLIDER JOINT',
            """<pre>SELECT TWO NODES AND TWO REFERENCE GEOMETRIES TO CREATE A SLIDER JOINT.\n
   An in-line joint and a prismatic joint will be added.</pre>""")                
        return {
            'Pixmap': __dir__ + '/icons/slider.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddSlider', _AddSliderCmd()) 

########################################################################################################################################################
#
#  Flexible:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////viscous body//////////////////////////////////////////////////////////"""
class _AddViscousBodyCmd:   
    def Activated(self):
        node = FreeCADGui.Selection.getSelection()[0]
        dyn.AddViscousBody(node) 

              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'VISCOUS BODY JOINT',
            'VISCOUS BODY JOINT')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'VISCOUS BODY JOINT',
            """<pre>SELECT ONE OR A GROUP OF NODES TO APPLY A VICOUS BODY JOINT(S).\n
   Viscous body elements define a force and a moment that depend on the absolute linear and angular velocity of
   a node, projected in the reference frame of the node itself. The force and moment are defined as a 6D viscous 
   constitutive law.
   Any of the supported 6D constitutive laws can be supplied to define the constitutive properties of each joint.
   If a 6D cosntitutive law is selected after the nodes, this law will be assigned to all the viscous body elements created.
   If no constitutive law is selected, one 6D cosntitutive law has to be manually assigned to each viscous body element.</pre>""")                
        return {
            'Pixmap': __dir__ + '/icons/honey.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddViscousBody', _AddViscousBodyCmd()) 

"""//////////////////////////////////////////////////////////////////////////////BEAM3//////////////////////////////////////////////////////////"""
class _AddBeam3Cmd:   
    def Activated(self):
    #try:
        aux1 = int(0)
        
        existingbeams = 1
        for obj1 in FreeCAD.ActiveDocument.Objects:#Count the existing joints            
            if obj1.Label.startswith('Beam:'):
                existingbeams = existingbeams+1
                
        obj2 = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Beam: "+str(existingbeams))
        
        obj2.Label = "Beam: "+str(existingbeams)
        FreeCAD.ActiveDocument.getObject("Beam_elements").addObject(obj2)
        #CustomView(obj2.ViewObject)
        
        if FreeCADGui.Selection.getSelection()[-1].Label.startswith("structural:"):
            aux = len(FreeCADGui.Selection.getSelection())-1
        
            while aux1 < aux:
                node1 = FreeCADGui.Selection.getSelection()[aux1]
                node2 = FreeCADGui.Selection.getSelection()[aux1 + 1]
                node3 = FreeCADGui.Selection.getSelection()[aux1 + 2]
                dyn.AddBeam3(node1, node2, node3, str(existingbeams))  
                aux1 = aux1 + 2
       
        if FreeCADGui.Selection.getSelection()[-1].Label.startswith("law:"):
            law = FreeCADGui.Selection.getSelection()[-1].Label
            aux = len(FreeCADGui.Selection.getSelection())-2
        
            while aux1 < aux:
                node1 = FreeCADGui.Selection.getSelection()[aux1]
                node2 = FreeCADGui.Selection.getSelection()[aux1 + 1]
                node3 = FreeCADGui.Selection.getSelection()[aux1 + 2]
                dyn.AddBeam3(node1, node2, node3, str(existingbeams), law)                     
                aux1 = aux1 + 2
                
        FreeCAD.ActiveDocument.recompute()
        #except:
         #   QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the beam3 element(s).
                                          
#Please make sure you have selected an odd number of three or more nodes.
#Alternatively, you can also select a 6D constitutive law after the nodes,
#the selected law will be applied to all the beam3 elements created.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'BEAM3',
            'BEAM3')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'BEAM3',
            """<pre>SELECT ANY ODD NUMBER OF THREE OR MORE NODES TO CREATE ONE OR A SET OF BEAM3 ELEMENTS.\n
   Beam3 elements allow to model slender deformable structural components with a high level of flexibility.
   A 6D constitutive law must be provided, which defines the relationship between the strains, the curvatures
   of the beam and their time derivatives, and the internal forces and moments at the evaluation points.
   Any of the supported 6D constitutive laws can be supplied to define the constitutive properties of each beam section.   
   If a 6D cosntitutive law is selected at the end of the nodes, this law will be applied to all the beam3 elements created
   If no constitutive law is selected, one 6D cosntitutive law has to be manually asociated to each beam3 element.</pre>""")                
        return {
            'Pixmap': __dir__ + '/icons/beam3.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddBeam3', _AddBeam3Cmd()) 

########################################################################################################################################################
#
#  Forces, couples and gravity:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL FORCE/////////////////////////////////////////////////////////////"""
class _AddStructuralForceCmd:    
    def Activated(self):
        #try:
        dyn.AddStructuralForce()
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding the structural force.
                                          
#Please make sure you have selected only one node or one node and a reference geometry.""")
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'STRUCTURAL FORCE',
            'STRUCTURAL FORCE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'STRUCTURAL FORCE',
            """<pre>SELECT ONE NODE TO APPLY A STRUCTURAL FORCE.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/force.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None
    
FreeCADGui.addCommand('MBdyn_AddStructuralForce', _AddStructuralForceCmd()) 

"""//////////////////////////////////////////////////////////////////////////////StructuralCouple////////////////////////////////////////////////////////////"""
class _AddStructuralCoupleCmd:    
    def Activated(self):
        #try:
        dyn.AddStructuralCouple()
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding the structural force.
                                          
#Please make sure you have selected only one node or one node and a reference geometry.""")
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'STRUCTURAL COUPLE',
            'STRUCTURAL COUPLE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'STRUCTURAL COUPLE',
            """<pre>SELECT ONE NODE TO APPLY A STRUTURAL COUPLE.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/couple.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStructuralCouple', _AddStructuralCoupleCmd()) 

"""//////////////////////////////////////////////////////////////////////////////GRAVITY///////////////////////////////////////////////////////////////////"""
class _AddGravityCmd:    
    def Activated(self):
        #try:
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==1):
            dyn.AddGravity(b[0])

        if(len(b)==0):
            dyn.AddGravity()
        #except:
            #QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding gravity to the simulation.
                                          
#Please make sure that you have created a "WORLD" before you add gravity.

#If you wish to create a uniform gravity, please make sure that nothing is selected.

#If you wish to create a central gravity, please make sure you have selected only one solid object.""")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'GRAVITY',
            'GRAVITY')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'GRAVITY',
            """<pre>CLICK TO CREATE A GRAVITY ELEMENT.\n
    a) If nothing is selected, a uniform gravitational pull will be aded.
    b) If a solid object is selected, a central gravity will be created 
       at the center of mass of the object.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/apple.svg',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddGravity', _AddGravityCmd()) 

########################################################################################################################################################
#
#  SCALAR FUNCTION, DRIVE, CONSTITUTIVE LAW AND PLUGIN VARIABLE:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////CREATE A SCALAR FUNCTION////////////////////////////////////////////////////"""
class _AddUserDefinedScalarFunctionCmd:   
    def Activated(self):
        #try: 
            reply = QtGui.QInputDialog.getText(None,"Dynamics","Enter the number of steps (an integer):")
            if reply[1]:
                npoints = int(reply[0])

            dyn.AddUserDefinedScalarFunction(npoints)
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the scalar function.
                                          
#Please make sure that you have created a "world" first.""")
                                                       
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'USER-DEFINED SCALAR FUNCTION',
            'USER-DEFINED SCALAR FUNCTION')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'USER-DEFINED SCALAR FUNCTION',
            """<pre>This will add an empty scalar function to the simulation.\n    
    The user has to manually input the range of the function after creating it.
    The default domain will be defined by the initial and final time of the simulation.
    The default independent variable will be the Time variable of the simulation.
    The user can modify the domain, range and independent variable, after creating the function.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/scalar.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddUserDefinedScalarFunction', _AddUserDefinedScalarFunctionCmd()) 

class _AddPolarScalarFunctionCmd:   
    def Activated(self):
        #try: 
        dyn.AddPolarScalarFunction()
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the scalar function.
                                          
#Please make sure that you have created a "world" first.""")
                                                       
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'POLAR SCALAR FUNCTION',
            'POLAR SCALAR FUNCTION')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'POLAR SCALAR FUNCTION',
            """<pre>Click to create a polar scalar function.\n    
    The range of the function will be defined by the linear distance between a node and a 
    number of points evenly distributed along the selected edges.
    The default domain will be defined by the initial and final time of the simulation.
    The default independent variable will be the Time variable of the simulation.
    The user can modify the domain, the range and the independent variable, 
    after creating the function.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/scalar1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddPolarScalarFunction', _AddPolarScalarFunctionCmd()) 

class _AddCartesianScalarFunctionCmd:   
    def Activated(self):
        #try: 
        dyn.AddCartesianScalarFunction()
        #except:
        #    QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the scalar function.
                                          
#Please make sure that you have created a "world" first.""")
                                                       
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'CARTESIAN SCALAR FUNCTION',
            'CARTESIAN SCALAR FUNCTION')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'CARTESIAN SCALAR FUNCTION',
            """<pre>Click to create a cartesian scalar function.\n    
    The range of the function will be defined by the X, Y or Z component of the distance between 
    a node and a number of points evenly distributed along the selected edges.
    The default domain will be defined by the initial and final time of the simulation.
    The default independent variable will be the Time variable of the simulation.
    The user can modify the domain, the range and the independent variable, 
    after creating the function.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/scalar2.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddCartesianScalarFunction', _AddCartesianScalarFunctionCmd()) 

"""//////////////////////////////////////////////////////////////////////////////CREATE A DRIVE////////////////////////////////////////////////////"""
class _AddDriveCmd:   
    def Activated(self):
        #try:
        dyn.AddDrive()
        #except:
            #QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the drive.
                                          
#Please make sure that you have created a "world" first.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'DRIVE',
            'DRIVE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'DRIVE',
            """<pre>ADD A DRIVE TO THE SIMULETION.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/drive.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDrive', _AddDriveCmd()) 

"""//////////////////////////////////////////////////////////////////////////////CONSTITUTIVE LAW////////////////////////////////////////////////////"""
class _AddConstitutiveLawCmd:   
    def Activated(self):
        try:
            dyn.AddLaw()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while adding the constitutive law.
                                          
Please make sure that you have created a "world" first.""")
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'CONSTITUTIVE LAW',
            'CONSTITUTIVE LAW')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'CONSTITUTIVE LAW',
            """<pre>ADD A CONSTITUTIVE LAW TO THE SIMULATION.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/law.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_ConstitutiveLaw', _AddConstitutiveLawCmd()) 

########################################################################################################################################################
#
#  TOOLS TO RELOCATE:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////RELOCATE TOOL////////////////////////////////////////////////////"""
class _RelocateCmd:   
    def Activated(self):
        try:
            dyn.Relocate()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the object.
                                          
Please make sure you have selected a node or an orientation line, and a reference geometry or a second node.""")            
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE 3D',
            'RELOCATE 3D')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE 3D',
            """<pre>SELECT A NODE AND:
                
    a) a second node or,
    b) a reference geometry, such as a point, line, arc, face, or a 3D body;
                
       the selected node will be placed at the center of mass (or the center point, 
       in the case of arcs) of the reference geometry.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/Place.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Relocate', _RelocateCmd()) 

"""//////////////////////////////////////////////////////////////////////////////RELOCATE X TOOL////////////////////////////////////////////////////"""

class _RelocateXCmd:   
    def Activated(self):
        try:
            objs = FreeCADGui.Selection.getSelection()
            dyn.RelocateX(objs)
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the X position of the object.
                                          
Please make sure you have selected two nodes or a node and a reference geometry.""") 
                  
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE X',
            'RELOCATE X')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE X',
            """<pre>SELECT:\n
        a) An orientation line. The line will be made parallel to the global X axis.
        b) Two nodes or one node and a reference geometry. The first node´s X position component will be made equal to
           the second node´s X position component; or to the X value of the reference´s center of mass or center point.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/Place-X.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RelocateX', _RelocateXCmd())

"""//////////////////////////////////////////////////////////////////////////////RELOCATE-Y TOOL////////////////////////////////////////////////////"""
class _RelocateYCmd:   
    def Activated(self):
        try:
            objs = FreeCADGui.Selection.getSelection()
            dyn.RelocateY(objs)     
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the Y position of the object.
                                          
Please make sure you have selected two nodes or a node and a reference geometry.""") 
                          
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE Y',
            'RELOCATE Y')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE Y',
            """<pre>SELECT:\n
        a) An orientation line. The line will be made parallel to the global Y axis.
        b) Two nodes or one node and a reference geometry. The first node´s Y position component will be made equal to
           the second node´s Y position component; or to the Y value of the reference´s center of mass or center point.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/Place-Y.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RelocateY', _RelocateYCmd()) 

"""//////////////////////////////////////////////////////////////////////////////RELOCATE-Z TOOL////////////////////////////////////////////////////"""
class _RelocateZCmd:   
    def Activated(self):
        try:
            objs = FreeCADGui.Selection.getSelection()
            dyn.RelocateZ(objs) 
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while relocating the Z position of the object.
                                          
Please make sure you have selected two nodes or a node and a reference geometry.""")            
              
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE Z',
            'RELOCATE Z')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'RELOCATE Z',
            """<pre>SELECT:\n
        a) An orientation line. The line will be made parallel to the global Z axis.
        b) Two nodes or one node and a reference geometry. The first node´s Z position component will be made equal to
           the second node´s Z position component; or to the Z value of the reference´s center of mass or center point.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/Place-Z.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RelocateZ', _RelocateZCmd()) 

########################################################################################################################################################
#
#  VARIOUS TOOLS:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////EYE///////////////////////////////////////////////////////////////////////"""
class _ViewCmd:   
    def Activated(self):
        try:
            s = FreeCADGui.Selection.getSelection()
            if len(s)==1:
                dyn.Hide(FreeCADGui.Selection.getSelection()[0])        
            else:
                dyn.View()
        except:
            QtGui.QMessageBox.information(None,"Error","""Something went wrong while hiding the objects.
                                          
Please make sure that you have selected a node or a joint.""")
                          
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'VIEW',
            'VIEW')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'VIEW',
            """<pre>
    a) Select a joint to hide all the nodes and bodies exept those linked to the joint selected or,
    b) select a body to hide all the joints and bodies that are not connected to this body or,             
    c) select nohing to make all the joints and bodies visible.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/eye.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_View', _ViewCmd()) 

"""///////////////////////////////////////////////////////////CREATE GLOBAL REFERENCE FRAME AND ADD CONTAINERS////////////////////////////////////////////"""
class _AddXYZCmd:    
    def Activated(self): 
        #try:         
        dyn.CreateWorld();
        #except:
            #QtGui.QMessageBox.information(None,"Error","""Something went wrong while creating the world.""")       

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'GLOBAL REFERENCE FRAME AND CONTAINERS',
            'GLOBAL REFERENCE FRAME AND CONTAINERS')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'GLOBAL REFERENCE FRAME AND CONTAINERS',
            """<pre>CLICK HERE TO:\n
    a) Create all the containers needed to organize the elements of the MBD simulation. These are: 
       rigid and flexible bodies, materials, forces, couples, joints and nodes, etc.
    b) Add an inertial global reference frame.
    c) Create the parameters for the MBD simulation and animation and post-processing of the
    simulation results.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/earth.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddXYZ', _AddXYZCmd())

"""//////////////////////////////////////////////////////////SHOW POSITION OF A NODE, JOINTOR BODY////////////////////////////////////////////////////"""
class _Cm1Cmd:  
    def Activated(self):
        X, Y, Z = 0, 0, 0 
        mX, mY, mZ = 0, 0, 0 
        try:
            length = FreeCAD.ActiveDocument.getObject("Line").End[0]
            
            if (len(FreeCADGui.Selection.getSelection()))==1:
                b = FreeCADGui.Selection.getSelection()
                #If a solid object was selected:
                x = b[0].Shape.Solids[0].CenterOfMass[0]
                y = b[0].Shape.Solids[0].CenterOfMass[1]
                z = b[0].Shape.Solids[0].CenterOfMass[2]                             
                #if a rigid body was selected    
                if(b[0].Label.startswith('body:')):
                    x = FreeCAD.Units.Quantity(b[0].absolute_center_of_mass_X).Value
                    y = FreeCAD.Units.Quantity(b[0].absolute_center_of_mass_Y).Value
                    z = FreeCAD.Units.Quantity(b[0].absolute_center_of_mass_Z).Value                                                    
    
                FreeCAD.ActiveDocument.cmx.X1=x+length
                FreeCAD.ActiveDocument.cmx.Y1=y
                FreeCAD.ActiveDocument.cmx.Z1=z
                FreeCAD.ActiveDocument.cmx.X2=x-length
                FreeCAD.ActiveDocument.cmx.Y2=y
                FreeCAD.ActiveDocument.cmx.Z2=z
                
                FreeCAD.ActiveDocument.cmy.X1=x
                FreeCAD.ActiveDocument.cmy.Y1=y+length
                FreeCAD.ActiveDocument.cmy.Z1=z
                FreeCAD.ActiveDocument.cmy.X2=x
                FreeCAD.ActiveDocument.cmy.Y2=y-length
                FreeCAD.ActiveDocument.cmy.Z2=z
                
                FreeCAD.ActiveDocument.cmz.X1=x
                FreeCAD.ActiveDocument.cmz.Y1=y
                FreeCAD.ActiveDocument.cmz.Z1=z+length
                FreeCAD.ActiveDocument.cmz.X2=x
                FreeCAD.ActiveDocument.cmz.Y2=y
                FreeCAD.ActiveDocument.cmz.Z2=z-length
                
                FreeCADGui.ActiveDocument.getObject('cmx').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmy').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmz').Visibility = True
            
            #if a set of objects were selected                        
            if (len(FreeCADGui.Selection.getSelection()))>1:
                
                for obj in range(0,len(FreeCADGui.Selection.getSelection())):
                    X = X + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[0] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))
                    Y = Y + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[1] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))
                    Z = Z + (FreeCADGui.Selection.getSelection()[obj].Shape.Solids[0].CenterOfMass[2] * float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0]))

                    mX = mX + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    mY = mY + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    mZ = mZ + float(FreeCADGui.Selection.getSelection()[obj].mass.split(" ")[0])
                    
                x = X / mX
                y = Y / mY
                z = Z / mZ
        
                FreeCAD.ActiveDocument.cmx.X1=x+length
                FreeCAD.ActiveDocument.cmx.Y1=y
                FreeCAD.ActiveDocument.cmx.Z1=z
                FreeCAD.ActiveDocument.cmx.X2=x-length
                FreeCAD.ActiveDocument.cmx.Y2=y
                FreeCAD.ActiveDocument.cmx.Z2=z
                
                FreeCAD.ActiveDocument.cmy.X1=x
                FreeCAD.ActiveDocument.cmy.Y1=y+length
                FreeCAD.ActiveDocument.cmy.Z1=z
                FreeCAD.ActiveDocument.cmy.X2=x
                FreeCAD.ActiveDocument.cmy.Y2=y-length
                FreeCAD.ActiveDocument.cmy.Z2=z
                
                FreeCAD.ActiveDocument.cmz.X1=x
                FreeCAD.ActiveDocument.cmz.Y1=y
                FreeCAD.ActiveDocument.cmz.Z1=z+length
                FreeCAD.ActiveDocument.cmz.X2=x
                FreeCAD.ActiveDocument.cmz.Y2=y
                FreeCAD.ActiveDocument.cmz.Z2=z-length
                
                FreeCADGui.ActiveDocument.getObject('cmx').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmy').Visibility = True
                FreeCADGui.ActiveDocument.getObject('cmz').Visibility = True
                
            if (len(FreeCADGui.Selection.getSelection()))==0:
                FreeCADGui.ActiveDocument.getObject('cmx').Visibility = False
                FreeCADGui.ActiveDocument.getObject('cmy').Visibility = False
                FreeCADGui.ActiveDocument.getObject('cmz').Visibility = False
                
        except:
            print(QtGui.QMessageBox.information(None,"Error","Select only one structural node or one or more rigid bodies."))
                                            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'HIGHLIGHT CENTER OF MASS OR BARYCENTER',
            'HIGHLIGHT CENTER OF MASS OR BARYCENTER')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'HIGHLIGHT CENTER OF MASS OR BARYCENTER',
            """<pre>HIGHLIGHT CENTER OF MASS OR BARYCENTER:\n
    Select a solid object or a rigid body to highlight its center of mass.
    If multiple solid objects or rigid bodies are selected, the barycenter will be calculated and highlited.
    If nothing is selected, the marking lines will hide.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/center_of_mass.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Cm1', _Cm1Cmd())

"""////////////////////////////////////////////////////////////////////RECALCULATE THE HOLE MODEL////////////////////////////////////////////////////"""
class _RecalculateOrientation:    
    def Activated(self):
        for obj in FreeCAD.ActiveDocument.Objects:
            obj.touch()
            
        FreeCAD.ActiveDocument.recompute()   

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'FORCE MODEL RECALCULATION',
            'FORCE MODEL RECALCULATION')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'FORCE MODEL RECALCULATION',
            """<pre>FORCE MODEL RECALCULATION:\n
    Forces the recalculation of the whole model.
    WARNING: Depending on the complexity of the MBD and the CAD models,
    the recalculation may require a considerable computation time.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/recalculate_node.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RecalculateOrientation', _RecalculateOrientation()) 

"""//////////////////////////////////////////////////////////////////////////////RANDOM COLOR////////////////////////////////////////////////////"""
class _RandomColorCmd:    
    def Activated(self):
        try:
            b = FreeCADGui.Selection.getSelection()
            dyn.RandomColor(b[0])   
        except:
            print(QtGui.QMessageBox.information(None,"Error", "Select only one rigid body."))
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'RANDOM COLOR',
            'RANDOM COLOR')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'RANDOM COLOR',
            """<pre>RANDOM COLOR:\n
    Select only one rigid body to give it a random color.
    It´s transparency will not change.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/colors.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_RandomColor', _RandomColorCmd()) 

"""//////////////////////////////////////////////////////////////////////////////REDIRECTION TOOL////////////////////////////////////////////////////"""
class _RedirectionCmd:   
    def Activated(self):
        try: 
            objs = FreeCADGui.Selection.getSelection()[0]
            dyn.Redirection(objs)
        except:
            print(QtGui.QMessageBox.information(None,"Error", "Select only one orientation vector."))
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'FLIP SENSE',
            'FLIP SENSE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'FLIP SENSE',
            """<pre>FLIP SENSE:\n
    Select only one orientation vector to flip (invert) it´s sense. 
    The vector´s direction will be kept constant.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/plusminus.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Redirection', _RedirectionCmd()) 

########################################################################################################################################################
#
#  TOOLS TO MANIPULATE JOINTS AND NODES:
#    
########################################################################################################################################################

"""//////////////////////////////////////////////////////////////////////////////PLUGIN VARIABLE////////////////////////////////////////////////////"""
class _AddPuginVariableCmd:   
    def Activated(self):
        s = FreeCADGui.Selection.getSelection()
        if len(s)==1:
            dyn.AddPlugin(FreeCADGui.Selection.getSelection()[0])        
        else:
            pass
                         
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'PLUGIN VARIABLE',
            'PLUGIN VARIABLE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'PLUGIN VARIABLE',
            """<pre>SELECT A NODE OR A JOINT TO ADD A PLUGIN VARIABLE.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/plugin.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddPuginVariable', _AddPuginVariableCmd()) 


"""//////////////////////////////////////////////////////////////////////////////INFO////////////////////////////////////////////////////"""
class _InfoCmd:  
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==0):
            QtGui.QMessageBox.information(None,"Error","Select a node or a joint first.")
        else:
            if(b[0].Label.startswith('structural:')):
                node = int(b[0].label)
                Infonode(node)#Show node's info
            
            if(b[0].Label.startswith('joint:')):
                joint = int(b[0].label)
                Infojoint(joint)#Show joint's info


    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'Export summarized simulation results to a FreeCAD spreadsheet',
            'Export summarized simulation results to a FreeCAD spreadsheet')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'Export summarized simulation results to a FreeCAD spreadsheet',
            '<pre>Export summarized simulation results to a FreeCAD spreadsheet.</pre>')
        return {
            'Pixmap': __dir__ + '/icons/spread1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Info', _InfoCmd())

"""//////////////////////////////////////////////////////////////////////////////SPREADSHEET////////////////////////////////////////////////////"""
class _SpreadsheetCmd:  
    def Activated(self):
        if(len(FreeCADGui.Selection.getSelection())==0):
            QtGui.QMessageBox.information(None,"Error","Select a node or a joint first.")
        else:
            choice = QtGui.QMessageBox.question(None,'Continue?',"Depending on your computer's resources and the simulation resolution, this may take some time. Hint: you can reduce the simulation resolution by increasing the time_step or reducing the final_time of the MBDyn object, in the MBDyn_simulation container. Continue?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

        if choice == QtGui.QMessageBox.Yes:
            Tospreadsheet(FreeCADGui.Selection.getSelection()[0])
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'Export full simulation results to a FreeCAD spreadsheet',
            'Export full simulation results to a FreeCAD spreadsheet')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'Export full simulation results to a FreeCAD spreadsheet',
            '<pre>Export full simulation results to a FreeCAD spreadsheet.</pre>')
        return {
            'Pixmap': __dir__ + '/icons/spread.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Spreadsheet', _SpreadsheetCmd())
"""//////////////////////////////////////////////////////////////////////////////START ANIMATION////////////////////////////////////////////////////"""
class _Animate1Cmd:  
    def Activated(self):
        #dyn.WriteInputFile()
        #dyn.Run()        
        dyn.StartAnimation()
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'START ANIMATION',
            'START ANIMATION')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'START ANIMATION',
            """<pre>Click here to animate the MBDyn simulation results. 
    Make sure you execute MBDyn before you animate the model.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/play1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Animate1', _Animate1Cmd()) 
"""//////////////////////////////////////////////////////////////////////////////STOP ANIMATION////////////////////////////////////////////////////"""
class _AnimateStopCmd:  
    def Activated(self):
        dyn.StopAnimation()        
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'STOP ANIMATION',
            'STOP ANIMATION')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'STOP ANIMATION',
            """<pre>Click here to stop the animation.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/stop.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('AnimateStopCmd', _AnimateStopCmd()) 
"""//////////////////////////////////////////////////////////////////////////////RESTORE ALL BODIES AND VECTORS TO THEIR POSSITIONS////////////////////////////////////////////////////"""
class _RestoreCmd:  
    def Activated(self):
        dyn.RestoreScene()
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'RESTORE 3D SCENE',
            'RESTORE 3D SCENE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'RESTORE 3D SCENE',
            """<pre>Click here to restore the 3D scene after it has been animated.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/restore.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Restore', _RestoreCmd()) 
"""//////////////////////////////////////////////////////////////////////////////EXECUTE MBDYN////////////////////////////////////////////////////"""
class _RunCmd:  
    def Activated(self):
        #dyn.WriteInputFile()
        dyn.Run()
       
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'EXECUTE MBDyn SIMULATION',
            'EXECUTE MBDyn SIMULATION')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'EXECUTE MBDyn SIMULATION',
"""<pre>Click here to execute the MBDyn simulation from the input file. 
    Please make sure you generate the input file before executing MBDyn.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/play.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Run', _RunCmd())  


"""//////////////////////////////////////////////////////////////////////////////WRITE INPUT FILE////////////////////////////////////////////"""
class _AddMBDCmd:
    def Activated(self):
        dyn.WriteInputFile()

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'WRITE INPUT FILE',
            'WRITE INPUT FILE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'WRITE INPUT FILE',
            """<pre>CLICK HERE TO GENERATE THE INPUT FILE FOR MBDyn.
            
    You will find the input file within the "MBDyn_simulation" container, in the tree view.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/mbd.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddMBD', _AddMBDCmd()) 


"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL STATIC NODE///////////////////////////////////////////"""
class _AddStructuralStaticCmd:    
    def Activated(self):
        
        if len(FreeCADGui.Selection.getSelection()) == 1:#In this case the eference is the same rigid body to which the node belongs    
            baseBody = FreeCADGui.Selection.getSelection()[0]
            referenceObject = FreeCADGui.Selection.getSelection()[0]
            referenceGeometry = FreeCADGui.Selection.getSelectionEx()[0]  
            dyn.AddStructuralStaticNode(baseBody, referenceObject, referenceGeometry) 
            
        if len(FreeCADGui.Selection.getSelection()) == 2:#In this case the reference is another body    
            baseBody = FreeCADGui.Selection.getSelection()[0]
            referenceObject = FreeCADGui.Selection.getSelection()[1]
            referenceGeometry = FreeCADGui.Selection.getSelectionEx()[1]
            dyn.AddStructuralStaticNode(baseBody, referenceObject, referenceGeometry) 
                              
        if len(FreeCADGui.Selection.getSelection()) > 2:# In this case many bodies have been selected, and nodes are automatically added at the center of mass
            for obj in FreeCADGui.Selection.getSelection():
                baseBody = obj
                referenceObject = obj
                referenceGeometry = 'Edge1'
                dyn.AddStructuralStaticNode(baseBody, referenceObject, referenceGeometry)
            
            pass
    
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'STATIC NODE',
            "STATIC NODE")
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'STATIC NODE',
            """<pre>SELECT ONE STATIC BODY AND A REFERENCE GEOMETRY, OR A GROUP OF THREE OR MORE STATIC BODIES, 
            TO CREATE ONE OR A SET OF STRUCTURAL STATIC NODES. 

                If one body and one reference geometry are selected, only one node will be assigned to the selected body.
                Initially, the node will be placed at the center of mass of the body. It can later be moved to a point 
                asociated to the reference geometry, by changing the "attachment_mode" property.
                
                If three or more bodies are selected, one node will be assigned to each body, and placed at its 
                center of mass.
                
                NOTE: You can afterwards set the initial position and orientation, as well as the initial velocity and 
                initial angular velocity of any node.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/StructuralStatic.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStructuralStatic', _AddStructuralStaticCmd()) 

"""//////////////////////////////////////////////////////////////////////////////PLOT////////////////////////////////////////////////////"""
class _PlotCmd:  
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)!=1):
            QtGui.QMessageBox.information(None,"Error","Select one node or one joint first.")
        else:
            if(b[0].Label.startswith('structural:')):
                dyn.PlotNode()#Plot all the data contained in the .mov file
            
            if(b[0].Label.startswith('joint:')):
                dyn.PlotJoint()#Plot all the data contained in the .mov file

            if(b[0].Label.startswith('abstract:')):
                node = b[0]                
                dyn.PlotAbstract(node)#Plot all the data contained in the .mov file
                
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'PLOT SIMULATION RESULTS',
            'PLOT SIMULATION RESULTS')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'PLOT SIMULATION RESULTS',
"""<pre>SELECT A NODE, A JOINT, OR ONE OR TWO ABSTRACT NODES,
    IN ORDER TO PLOT THE SIMULATION RESULTS. 

NOTE:
    If two abstract nodes are selected, the first node will be used as the  
    domain of the plot, and the second the range.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/Matplotlib1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_Plot', _PlotCmd())

"""//////////////////////////////////////////////////////////////////////////////PLOT SCALAR FUNCTIONS OR DRIVES////////////////////////////////////////////////////"""
class _PlotDriveCmd:  
    def Activated(self):  
        obj = FreeCADGui.Selection.getSelection()[0]        
        dyn.PlotDrive(obj)#Plot all the data contained in the .mov file
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'PLOT SCALAR FUNCTION OR DRIVE',
            'PLOT SCALAR FUNCTION OR DRIVE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'PLOT SCALAR FUNCTION OR DRIVE',
"""<pre>SELECT A SCALAR FUNCTION OR A DRIVE TO PLOT IT.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/Matplotlib.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_PlotDrive', _PlotDriveCmd())



"""//////////////////////////////////////////////////////////////////////////////STATIC BODY////////////////////////////////////////////////////"""
class _AddStaticBodyCmd:
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==1):    
            dyn.AddStaticBody(b[0])            
        else:
            QtGui.QMessageBox.information(None,"Dynamics", "Dummy bodies do not have mass or inertia. They are only required to visualize the motion of dummy nodes. Select a solid object first. A dummy body associated to it's respective dummy node will be created.")
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'STATIC BODY',
            'STATIC BODY')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'STATIC BODY',
            """<pre>SELECT ONLY ONE SOLID OBJECT TO CREATE A STATIC BODY. 
            
    Unlike rigid bodies, static bodies do not have any 
    physical property (density, mass or inertia moments).
    In the simulation, static bodies are asociated to static nodes.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/viga2.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStaticBody', _AddStaticBodyCmd())
"""//////////////////////////////////////////////////////////////////////////////DUMMY BODY////////////////////////////////////////////////////"""
class _AddDummyBodyCmd:
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)==1):    
            dyn.AddDummyBody(b[0])
                    
        else:
            QtGui.QMessageBox.information(None,"Dynamics", "Dummy bodies do not have mass or inertia. They are only required to visualize the motion of dummy nodes. Select a solid object first. A dummy body associated to it's respective dummy node will be created.")
            
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'DUMMY BODY',
            'DUMMY BODY')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'DUMMY BODY',
            """<pre>SELECT ONLY ONE SOLID OBJECT TO CREATE A DUMMY BODY. 
            
    Unlike rigid bodies, dummy bodies do not have any physical property (density, mass or inertia moments).
    A dummy body serves as an indicator, as a way to visualize the motion of a dummy node when the results
    of the simulation are animated</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/viga1.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDummyBody', _AddDummyBodyCmd())

"""//////////////////////////////////////////////////////////////////////////////RIGID BODY///////////////////////////////////////////""" 
class _AddRigidBodyCmd:
    def Activated(self):
        #try:
        for obj in FreeCADGui.Selection.getSelection():
            dyn.AddRigidBody(obj)   
        #except:
        #    QtGui.QMessageBox.information(None,"FreeDyn","A structural node provides the degrees of freedom for a rigid body but it does not define a body. To define a body, mass, center of mass, and moments of inertia are required. A rigid body carries this information.\n\n Hint: select a simple (non-parametric) solid object first, enter the material's density, and FreeDyn will calculate the body's volume, mass, and moments of inertia. The rigid body will inherit the shape of the original solid object, and will be placed in the Rigid_bodies container, within Bodies.")

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'RIGID BODY',
            'RIGID BODY')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'RIGID BODY',
            """<pre>SELECT ONE OR A GROUP OF SOLID OBJECTS TO CREATE ONE OR MORE RIGID BODIES.\n    
    Each rigid body will be assigned the average density of steel, then, the mass 
    and moments of inertia will be calculated from the CAD geometry of the body. 
    You can afterwards change the body´s material, in order to change it´s
    physical properties.
    If the desired material is not in the list, the FreeCAD´s material editor can be launched
    to add new materials.</pre>""")           
        return {
            'Pixmap': __dir__ + '/icons/viga.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddRigidBody', _AddRigidBodyCmd())  

"""//////////////////////////////////////////////////////////////////////////////GEAR (A RIGID BODY plus some gear properties (RADIUS, teeth, modulus, etc)///////////////////////////////////////////""" 
class _AddGearCmd:
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()
        if(len(b)!=1):
            QtGui.QMessageBox.information(None,"FreeDyn","A structural node provides the degrees of freedom for a rigid body but it does not define a body. To define a body, mass, center of mass, and moments of inertia are required. A rigid body carries this information.\n\n Hint: select a simple (non-parametric) solid object first, enter the material's density, and FreeDyn will calculate the body's volume, mass, and moments of inertia. The rigid body will inherit the shape of the original solid object, and will be placed in the Rigid_bodies container, within Bodies.")
        else:
            dyn.AddGear(b[0]) 

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'GEAR',
            'GEAR')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'GEAR',
            """<pre>SELECT ONE OR A GROUP OF SOLID OBJECTS TO CREATE ONE OR MORE GEARS.\n 
    A gear object has all the properties of a rigid body plus number of teeth, 
    radius, module, and stiffnes coefficient of ground spring. 
    These variables can optionally be provided or left blank.
    These variables can afterwards be used in any mathematical expression, in conjunction 
    with a drives, scalar functions, plugin variables and user-defined variables.</pre>""")           
        return {
            'Pixmap': __dir__ + '/icons/gear.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddGear', _AddGearCmd())  

"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL DUMMY NODE////////////////////////////////////////////////////"""
class _AddDummyNodeCmd:  
    def Activated(self):
        b = FreeCADGui.Selection.getSelection()   
        if(len(b)==2):
            dyn.AddStructuralDumyNode(FreeCADGui.Selection.getSelection()[0],FreeCADGui.Selection.getSelection()[1])           
        else:
            QtGui.QMessageBox.information(None,"FreeDyn","Unlike dynamic nodes, a dummy node cannot assume any degree of freedom, and has to be attached to another node. Hint: select a structural dynamic node and a solid object firts. A dummy node will be attached to the selected dynamic node, and will be positioned at the center of mass of the solid object.")
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'DUMMY NODE',
            'DUMMY NODE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'DUMMY NODE',
            """<pre>SELECT ONE DYNAMIC NODE AND ONE REFERENCE GEOMETRY, IN THIS ORDER, TO ADD A DUMMY NODE.\n
    The dummy node will be placed at the center of mass of the reference geometry provided, and will be 
    attached to the dynamic node selected.\n
    Note: a dummy body must be created before addind the dummy node.
    </pre>""")
        return {
            'Pixmap': __dir__ + '/icons/StructuralDummy.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddDummyNode', _AddDummyNodeCmd())

"""//////////////////////////////////////////////////////////////////////////////ABSTRACT NODE////////////////////////////////////////////////////"""
class _AddAbstractNodeCmd:  
    def Activated(self):
        try:
            dyn.AddAbstractNode()  
        except:
            QtGui.QMessageBox.information(None,"Error","Something went wrong while creating the abstract node element.")         
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'ABSTRACT NODE',
            'ABSTRACT NODE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'ABSTRACT NODE',
            """<pre>CLICK HERE TO ADD AN ABSTRACT NODE TO THE SIMULATION.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/abstract.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddAbstractNodeCmd', _AddAbstractNodeCmd())

"""//////////////////////////////////////////////////////////////////////////////GNEL: CLAMP////////////////////////////////////////////////////"""
class _AddGenelClampCmd:  
    def Activated(self):
        #try:
        node = FreeCADGui.Selection.getSelection()[0]
        dyn.AddGenelClamp(node)  
        #except:
        #    QtGui.QMessageBox.information(None,"Error","Something went wrong while creating the abstract node element.")         
        
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'GENEL CLAMP',
            'GENEL CLAMP')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'GENEL CLAMP',
            """<pre>SELECT A NODE TO CREATE A GENEL CLAMP.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/genelclamp.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddGenelClampCmd', _AddGenelClampCmd())

"""//////////////////////////////////////////////////////////////////////////////STRUCTURAL DYNAMIC NODE///////////////////////////////////////////////////////////////"""
class _AddStructuralDynamicNodeCmd:  
    def Activated(self):
        #try:
           
        if len(FreeCADGui.Selection.getSelection()) == 1:#In this case the eference is the same rigid body to which the node belongs    
            baseBody = FreeCADGui.Selection.getSelection()[0]
            referenceObject = FreeCADGui.Selection.getSelection()[0]
            referenceGeometry = FreeCADGui.Selection.getSelectionEx()[0]  
            dyn.AddStructuralDynamicNode(baseBody, referenceObject, referenceGeometry) 
            
        if len(FreeCADGui.Selection.getSelection()) == 2:#In this case the reference is another body    
            baseBody = FreeCADGui.Selection.getSelection()[0]
            referenceObject = FreeCADGui.Selection.getSelection()[1]
            referenceGeometry = FreeCADGui.Selection.getSelectionEx()[1]
            dyn.AddStructuralDynamicNode(baseBody, referenceObject, referenceGeometry) 
                              
        if len(FreeCADGui.Selection.getSelection()) > 2:# In this case many bodies have been selected, and nodes are automatically added at the center of mass
            for obj in FreeCADGui.Selection.getSelection():
                baseBody = obj
                referenceObject = obj
                referenceGeometry = 'Edge1'
                dyn.AddStructuralDynamicNode(baseBody, referenceObject, referenceGeometry)
            
            pass
        #except:
         #   QtGui.QMessageBox.information(None,"FreeDyn","A structural node is a point in space which has six degrees of freedom, three define its possition, using cartesian coordinates (x,y,z), and three define its orientation, using Euler angles (Yaw, Pitch, Roll). A structural node is dynamic when it has inertia (linear and angular momentum). Hint: select solid object first. A structural dynamic node will be created at the center of mass of the object, and added to the Dynamic_nodes container, inside Structural_nodes. You can change the node's initial conditions in the node's properties.")
           
    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'DYNAMIC NODE',
            'DYNAMIC NODE')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'DYNAMIC NODE',
"""<pre>SELECT ONE RIGID BODY AND A REFERENCE GEOMETRY, OR A GROUP OF THREE OR MORE RIGID BODIES, 
TO CREATE ONE OR A SET OF STRUCTURAL DYNAMIC NODES. 

    If one body and one reference geometry are selected, only one node will be assigned to the selected body.
    Initially, the node will be placed at the center of mass of the body. It can later be moved to a point 
    asociated to the reference geometry, by changing the "attachment_mode" property.
    
    If three or more bodies are selected, one node will be assigned to each body, and placed at its 
    center of mass.
    
    NOTE: You can afterwards set the initial position and orientation, as well as the initial velocity and 
    initial angular velocity of any node.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/StructuralDynamic.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AddStructuralDynamicNodeCmd', _AddStructuralDynamicNodeCmd())


"""//////////////////////////////////////////////////////////////////////////////Check for joint´s consistency//////////////////////////////////////////////////"""
class _AutoReOrganizeCmd:
    
    def Activated(self):          
        dyn.AutoReOrganize();

    def GetResources(self):
        MenuText = QtCore.QT_TRANSLATE_NOOP(
            'JOINTS NAMING CONSISTENCY',
            'JOINTS NAMING CONSISTENCY')
        ToolTip = QtCore.QT_TRANSLATE_NOOP(
            'JOINTS NAMING CONSISTENCY',
            """<pre>Checks for joint´s naming consistency.</pre>""")
        return {
            'Pixmap': __dir__ + '/icons/organize.png',
            'MenuText': MenuText,
            'ToolTip': ToolTip}

    def IsActive(self):
        return not FreeCAD.ActiveDocument is None

FreeCADGui.addCommand('MBdyn_AutoReOrganize', _AutoReOrganizeCmd())

